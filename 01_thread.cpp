#include <iostream>
#include <thread>

void threadFunction()
{
    std::cout << "Hello from thread." << std::endl;
}

int main()
{
    std::thread th(&threadFunc);
    std::cout << "Hello from main thread." << std::endl;

    th.join();

    return 0;
}
