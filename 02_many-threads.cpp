#include <iostream>
#include <thread>
#include <vector>
#include <algorithm>

int main()
{
    std::vector<std::thread> workers;

    for (int i = 0; i < 10; ++i)
    {
        workers.push_back(std::thread([i]()
        {
            std::cout << "Hello from worker thread " << i << std::endl;
        }));
    }
    
    std::cout << "Hello from main thread" << std::endl;
    
    // Barrier.
    std::for_each(workers.begin(), workers.end(), [](std::thread &th)
    {
        th.join();
    });

    return 0;
}
