#include <iostream>
#include <thread>
#include <vector>
#include <algorithm>
#include <cassert>

void threadFunc(int i)
{
    std::cout << "Hello from worker thread " << i << std::endl;
}

int main()
{
    std::vector<std::thread> workers;

    for (int i = 0; i < 10; ++i)
    {
        auto th = std::thread(&threadFunc, i);
        workers.push_back(std::move(th));
    }
    
    std::cout << "Hello from main thread" << std::endl;
    
    // Barrier.
    std::for_each(workers.begin(), workers.end(), [](std::thread &th)
    {
        assert(th.joinable());
        th.join();
    });

    return 0;
}
