#include <iostream>
#include <string>
#include <thread>
#include <future>

void threadFunc(std::promise<std::string> &&promise)
{
    std::string str = "Hello from future.";
    promise.set_value(str);
}

int main()
{
    std::promise<std::string> promise;
    std::future<std::string> future = promise.get_future();

    std::thread th(&threadFunc, std::move(promise));
    std::cout << "Hello from main." << std::endl;

    std::string str = future.get();
    std::cout << str << std::endl;

    th.join();
    
    return 0;
}
