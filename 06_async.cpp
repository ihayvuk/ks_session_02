#include <iostream>
#include <string>
#include <thread>
#include <future>

std::string threadFunc()
{
    std::string str = "Hello from future.";
    return str;
}

int main()
{
    auto future = std::async(&threadFunc);
    
    std::cout << "Hello from main." << std::endl;

    std::string str = future.get();

    std::cout << str << std::endl;

    return 0;
}
