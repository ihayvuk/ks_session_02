CC = g++

LIBS = -lstdc++ -pthread

%:
	$(CC) -g -O0 -std=c++11 -o $*.out $*.cpp $(LIBS)

clean:
	rm -f *.out
